import {Directive, ElementRef, EventEmitter, HostListener, Input, Output} from '@angular/core';

@Directive({
  selector: '[name]'
})
export class NameDirective {
  @Input('name') input: string;
  @Output('nameChange') nameEvent = new EventEmitter<string>();

  constructor(private elRef: ElementRef<HTMLDivElement>) { }

  @HostListener('mouseover') onMouseOver() {
    this.nameEvent.emit('Blop');
  }

}
