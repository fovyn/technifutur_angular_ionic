import {AfterViewInit, Directive, Input, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[myFor]'
})
export class MyForDirective implements OnInit, AfterViewInit {
  @Input('myForOf') data: Array<string>;
  @Input('myForWithUpper') upper: boolean = false;

  constructor(private container: ViewContainerRef, private template: TemplateRef<any>) { }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    for (const item of this.data) {
      this.container.createEmbeddedView(this.template, {$implicit: this.upper ? item.toUpperCase() : item});
    }
  }

}
