import {AfterContentInit, AfterViewInit, Directive, ElementRef, EventEmitter, Input, OnDestroy, Output} from '@angular/core';

declare const M: any;

@Directive({
  selector: '[mCollapsible]'
})
export class MCollapsibleDirective implements AfterContentInit, OnDestroy {
  @Input('mCollapsible') data: any;
  @Output('mCollapsibleChange') mCollapsibleEvent = new EventEmitter<any>();
  private instance: any;

  constructor(private element: ElementRef<HTMLUListElement>) { }

  ngAfterContentInit(): void {
    this.Instance = M.Collapsible.init(this.element.nativeElement);
    this.mCollapsibleEvent.emit(this.Instance);
  }

  ngOnDestroy(): void {
    this.instance.destroy();
  }

  get Instance(): any { return this.instance; }
  set Instance(v: any) { this.instance = v; }

}
