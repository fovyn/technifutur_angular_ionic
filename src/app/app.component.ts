import {Component, ComponentFactory, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {FORM_PERSON_CREATE} from './forms/person.form';
import {FormControl, Validators} from '@angular/forms';
import {ModalComponent} from './components/modal/modal.component';
import {PersonService} from './person/services/person.service';

export interface Person {name: string; firstname: string; age: number; }

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild('modal', {read: ViewContainerRef}) container;

  title = 'TechnifuturPetShop';
  today = new Date();

  componentRef: any;

  constructor(private resolver: ComponentFactoryResolver) {}

  ngOnInit(): void {

  }

  openModal() {
  }
}
