import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MCollapsibleDirective } from './directives/m-collapsible.directive';
import { NameDirective } from './directives/name.directive';
import { MyForDirective } from './directives/my-for.directive';
import { ListComponent } from './components/list/list.component';
import { CreateComponent } from './components/create/create.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RegisterComponent } from './components/register/register.component';
import { ModalComponent } from './components/modal/modal.component';
import { ValidationPopupComponent } from './components/validation-popup/validation-popup.component';
import { PopupDirective } from './directives/popup.directive';
import { PaginationPipe } from './pipes/pagination.pipe';
import { PaginationComponent } from './components/pagination/pagination.component';
import {PersonModule} from './person/person.module';

@NgModule({
  declarations: [
    AppComponent,
    MCollapsibleDirective,
    NameDirective,
    MyForDirective,
    ListComponent,
    CreateComponent,
    RegisterComponent,
    ModalComponent,
    PopupDirective,
    PaginationPipe,
    PaginationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PersonModule,
  ],
  entryComponents: [ModalComponent, ValidationPopupComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
