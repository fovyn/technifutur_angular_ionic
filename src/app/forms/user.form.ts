import {AbstractControl, FormArray, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
export interface FormType {
  [key: string]: AbstractControl;
}

export function same(input1: string, input2: string, errorName: string): ValidatorFn {
  return (formGroup) => {
    const input1Value = formGroup.get(input1);
    const input2Value = formGroup.get(input2);
    const errors = {};
    errors[errorName] = input1Value === input2Value;
    return errors[errorName] ? null : errors;
  };
}

export const FORM_USER_REGISTER: FormType = {
  username: new FormControl('', [Validators.required]),
  password: new FormControl('', [Validators.required]),
  confirmPassword: new FormControl('', [Validators.required]),
  email: new FormControl('', [Validators.required]),
  confirmEmail: new FormControl('', [Validators.required]),
  phoneNumbers: new FormArray([], []),
};

