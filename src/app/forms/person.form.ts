import {AbstractControl, FormArray, FormControl, Validators} from '@angular/forms';

export const FORM_PERSON_CREATE: {[key: string]: AbstractControl} = {
  name: new FormControl('', [Validators.required, Validators.minLength(4)]),
  firstname: new FormControl('', [Validators.required]),
  age: new FormControl(0, [Validators.min(0), Validators.max(132)])
};
