import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pagination'
})
export class PaginationPipe implements PipeTransform {

  transform(value: any[], startIndex: number, nbItemPerPage: number): unknown {
    return value.slice(startIndex, startIndex + nbItemPerPage);
  }

}
