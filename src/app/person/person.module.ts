import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonRoutingModule } from './person-routing.module';
import { PersonComponent } from './components/person/person.component';
import { ListComponent } from './components/list/list.component';
import { DetailComponent } from './components/detail/detail.component';
import {PersonResolver} from './resolvers/person.resolver';
import {PersonsResolver} from './resolvers/persons.resolver';
import { LoginComponent } from './components/login/login.component';
import {OvynGuard} from './guards/ovyn.guard';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  declarations: [PersonComponent, ListComponent, DetailComponent, LoginComponent],
  imports: [
    CommonModule,
    PersonRoutingModule,
    HttpClientModule,
  ],
  providers: [
    PersonResolver,
    PersonsResolver,
  ],
  exports: []
})
export class PersonModule { }
