import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Person} from '../../../app.component';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  person: Person;
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    console.log(this.activatedRoute);
    this.activatedRoute.data.subscribe(({person}: any) => this.person = person);
  }

}
