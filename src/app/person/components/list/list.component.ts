import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Data} from '@angular/router';
import {Observable} from 'rxjs';
import {Person} from '../../../app.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  data: any;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.data = (this.activatedRoute.snapshot.data as {persons: Observable<any[]>}).persons;
  }

}
