import { Component, OnInit } from '@angular/core';
import {SessionService} from '../../services/session.service';
import {Person} from '../../../app.component';
import {PokeService} from '../../services/poke.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private sessionService: SessionService, private pokeService: PokeService) { }

  ngOnInit(): void {
    this.sessionService.setUser({name: 'Ovyn', firstname: 'Flavian'} as Person);
    this.pokeService.pokemons$.subscribe(data => console.log(data));
  }

}
