import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PersonComponent} from './components/person/person.component';
import {ListComponent} from './components/list/list.component';
import {DetailComponent} from './components/detail/detail.component';
import {PersonResolver} from './resolvers/person.resolver';
import {PersonsResolver} from './resolvers/persons.resolver';
import {OvynGuard} from './guards/ovyn.guard';
import {LoginComponent} from './components/login/login.component';


const routes: Routes = [
  { path: '', component: PersonComponent, children: [
    { path: 'login', component: LoginComponent},
      {path: 'list', component: ListComponent, resolve: { persons: PersonsResolver } },
      { path: ':id', component: DetailComponent, resolve: { person: PersonResolver}, canActivate: [OvynGuard] }
    ] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonRoutingModule { }
