import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Person} from '../../app.component';
import {Observable} from 'rxjs';
import {PersonService} from '../services/person.service';
import {PokeService} from '../services/poke.service';

@Injectable()
export class PersonsResolver implements Resolve<any> {
  constructor(private pokeService: PokeService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any>{
    return this.pokeService.pokemons$;
  }

}
