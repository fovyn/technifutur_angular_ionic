import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Person} from '../../app.component';
import {Observable} from 'rxjs';
import {PersonService} from '../services/person.service';
import {filter} from 'rxjs/operators';

@Injectable()
export class PersonResolver implements Resolve<Person> {
  constructor(private personService: PersonService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Person> | Promise<Person> | Person {
    const id = route.params.id;
    return this.personService.getPerson(id);
  }
}
