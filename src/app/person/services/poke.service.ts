import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PokeService {

  constructor(private httpClient: HttpClient) { }

  get pokemons$(): Observable<any> {
    return this.httpClient.get<any>(`https://api.irail.be/stations/?format=json`);
  }
}
