import { Injectable } from '@angular/core';
import {Person} from '../../app.component';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  private personSubject$: BehaviorSubject<Person[]> = new BehaviorSubject<Person[]>([]);

  constructor() { }

  get Persons$(): Observable<Person[]> {
    return this.personSubject$.asObservable();
  }

  addPerson(person: Person) {
    const before = this.personSubject$.getValue();
    this.personSubject$.next([...before, person]);
  }

  removePerson(person: Person) {
    const before = this.personSubject$.getValue();

    const index = before.findIndex((p: Person) => p.name === person.name);
    if (index !== -1) {
      before.splice(index, 1);
      this.personSubject$.next([...before]);
    }
  }

  getPerson(id: string) {
    return this.personSubject$.getValue().find(p => p.name === id);
  }

  getPersons() {
    return this.personSubject$.getValue();
  }
}
