import { Injectable } from '@angular/core';
import {Person} from '../../app.component';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }

  getUser(): Person {
    return JSON.parse(sessionStorage.getItem('user')) as Person;
  }

  setUser(v: Person) {
    sessionStorage.setItem('user', JSON.stringify(v));
  }
}
