import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivateChild} from '@angular/router';
import { Observable } from 'rxjs';
import {PersonService} from '../services/person.service';
import {SessionService} from '../services/session.service';

@Injectable({ providedIn: 'root'})
export class OvynGuard implements CanActivateChild {
  constructor(private sessionService: SessionService) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return this.sessionService.getUser() && this.sessionService.getUser().name === 'Ovyn';
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.sessionService.getUser() && this.sessionService.getUser().name === 'Ovyn';
  }


}
