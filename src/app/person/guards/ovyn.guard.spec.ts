import { TestBed } from '@angular/core/testing';

import { OvynGuard } from './ovyn.guard';

describe('OvynGuard', () => {
  let guard: OvynGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(OvynGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
