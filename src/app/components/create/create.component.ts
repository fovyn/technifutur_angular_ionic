import {AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Person} from '../../app.component';
import {FormType} from '../../forms/user.form';
import {FORM_PERSON_CREATE} from '../../forms/person.form';
import {PersonService} from '../../person/services/person.service';

@Component({
  selector: 'create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit, AfterViewInit, OnChanges {
  form: FormGroup;
  phoneNumbers: FormArray;

  constructor(private builder: FormBuilder, private personService: PersonService) { }

  ngOnInit(): void {
    this.form = this.builder.group(FORM_PERSON_CREATE);
    this.phoneNumbers = this.form.get('phoneNumbers') as FormArray;
  }

  ngOnChanges({edit}: SimpleChanges): void {

  }

  ngAfterViewInit(): void {

  }

  submitAction() {
    if (this.form.valid) {
      console.log(this.form.value);
      this.personService.addPerson(this.form.value as Person);
    }
  }

  addPhoneNumber() {
    (this.form.get('phoneNumbers') as FormArray).push(new FormControl(''));

    console.log(this.form.get('phoneNumbers'));
  }
}
