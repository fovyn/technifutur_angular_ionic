import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'validation-popup',
  templateUrl: './validation-popup.component.html',
  styleUrls: ['./validation-popup.component.scss']
})
export class ValidationPopupComponent implements OnInit {
  @Output() validEvent = new EventEmitter<void>();
  @Output() abordEvent = new EventEmitter<void>();
  constructor() { }

  ngOnInit(): void {
  }

  validAction() {
    this.validEvent.emit();
  }

  abordAction() {
    this.abordEvent.emit();
  }
}
