import {Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {FORM_USER_REGISTER, same} from '../../forms/user.form';
import {ValidationPopupComponent} from '../validation-popup/validation-popup.component';
import {PopupDirective} from '../../directives/popup.directive';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @ViewChild(PopupDirective, {read: ViewContainerRef}) container: ViewContainerRef;

  registerForm: FormGroup;
  phoneNumbers: FormArray;

  constructor(private builder: FormBuilder, private resolver: ComponentFactoryResolver) { }

  ngOnInit(): void {
    this.registerForm = this.builder.group(FORM_USER_REGISTER, {validators: [
      same('password', 'confirmPassword', 'passwordNotSame'),
      same('email', 'confirumEmail', 'emailNotSame')
    ]});
    this.phoneNumbers = this.registerForm.get('phoneNumbers') as FormArray;
  }

  registerAction() {
    const factory = this.resolver.resolveComponentFactory<ValidationPopupComponent>(ValidationPopupComponent);
    console.log(this.container);
    const component = this.container.createComponent(factory);

    component.instance.validEvent.subscribe(() => {
      console.log(this.registerForm.value);
      component.instance.validEvent.unsubscribe();
      component.destroy();
    });

    component.instance.abordEvent.subscribe(() => {
      console.log('Abord');
      component.instance.validEvent.unsubscribe();
      component.destroy();
    });
  }

  addPhoneNumber() {
    (this.registerForm.get('phoneNumbers') as FormArray).push(new FormControl(''));

    console.log(this.registerForm.get('phoneNumbers'));
  }
}
