import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {PersonService} from '../../person/services/person.service';
import {Person} from '../../app.component';
import {Observable, range, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnChanges, OnDestroy {
  @Input() columns: Array<string> = [];
  @Input() actions: Array<'edit'|'delete'> = [];
  @Output('edit') editEvent = new EventEmitter<any>();
  @Output('delete') deleteEvent = new EventEmitter<any>();
  data: Observable<Array<Person>>;
  currentPage = 0;
  nbItemPerPage = 5;
  private personSubscription: Subscription;

  constructor(private personService: PersonService) { }

  ngOnInit(): void {
    this.data = this.personService.Persons$;
    this.personSubscription = this.data.subscribe();
    this.data.pipe(map(data => console.log(data))).subscribe();
  }

  ngOnChanges({}: SimpleChanges): void {
  }
  ngOnDestroy(): void {
    this.personSubscription.unsubscribe();
  }


  editAction(item: any) {
    this.editEvent.emit(item);
  }

  deleteAction(item: any) {
    this.personService.removePerson(item as Person);
    this.deleteEvent.emit(item);
  }


}
