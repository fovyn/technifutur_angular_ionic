import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {from, Observable, of, range} from 'rxjs';
import {last} from 'rxjs/operators';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  length: number = 0;
  @Input('current') currentPageInput = 0;
  @Output('currentChange') currentPageChangeEvent = new EventEmitter<number>();
  @Input('nbItemPerPage') nbItemPerPageInput = 5;

  constructor() { }

  ngOnInit(): void {
  }

  get Range(): Array<number> {
    const length = this.length;
    const ranges = [];
    let nbPage = parseInt((length / this.nbItemPerPageInput).toString(), 0);
    const rest = length % this.nbItemPerPageInput;

    if (rest !== 0) {
      nbPage++;
    }

    range(1, nbPage).subscribe(data => ranges.push(data));

    return ranges;
  }
  @Input()
  set Length(v: any[]) {
    this.length = v.length;
  }

  getLastPage(): Observable<number> {
    const obs = from(this.Range);
    return obs.pipe(last());
  }

  previousAction() {
    this.currentPageInput -= this.nbItemPerPageInput;
    this.currentPageChangeEvent.emit(this.currentPageInput);
  }

  nextAction() {
    this.currentPageInput += this.nbItemPerPageInput;
    this.currentPageChangeEvent.emit(this.currentPageInput);
  }

  goToPage(page: number) {
    this.currentPageInput = --page * this.nbItemPerPageInput;
    this.currentPageChangeEvent.emit(this.currentPageInput);
  }
}
