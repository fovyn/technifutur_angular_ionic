import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  @Input() message: string;
  @Output('out') outEvent = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
  }

  closeAction() {
    this.outEvent.emit();
  }
}
