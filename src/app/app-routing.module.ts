import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PersonModule} from './person/person.module';
import {OvynGuard} from './person/guards/ovyn.guard';


const routes: Routes = [
  { path: 'person', loadChildren: () => import('./person/person.module').then(m => m.PersonModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
